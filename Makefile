.PHONY: all clean lib src run

all: lib src


lib:
	$(MAKE) -C ./lib/tuya-iot-core-sdk

src:
	$(MAKE) -C ./src

clean: clean_lib clean_src


clean_lib:
	$(MAKE) -C ./lib/tuya-iot-core-sdk clean

clean_src:
	$(MAKE) -C ./src clean

run:
	./src/build/first_daemon_program -i "26c28934e0c87ef8747n5h" -s "ba1e1d8bf9278801" -p "bkgdyaysaiyyqeia" -t 10

run_daemonize:
	./src/build/first_daemon_program -i "26c28934e0c87ef8747n5h" -s "ba1e1d8bf9278801" -p "bkgdyaysaiyyqeia" -t 10 -d

valgrind:
	valgrind --leak-check=full --show-leak-kinds=all --track-origins=yes --verbose --log-file=valgrind-out.txt ./src/build/first_daemon_program -i "26c28934e0c87ef8747n5h" -s "ba1e1d8bf9278801" -p "bkgdyaysaiyyqeia" -t 10

# LD_LIBRARY_PATH=~/projects/first_daemon_program/lib/tuya-iot-core-sdk/build/lib:$LD_LIBRARY_PATH
# export LD_LIBRARY_PATH
