# first_daemon_program

## Documentation

* [Makefile](#makefile)
* [How to run](#how-to-run)
* [Useful terminal commands](#useful-terminal-commands)
* [Clouds commands](#clouds-commands)
## Makefile

    The following describes the make commands:

- `all`: Build shared library and src.
- `lib`: Build shared library.
- `src `: Build src.
- `clean `: Delete library and src build files.
- `clean_lib `: Delete library build files.
- `clean_src `: Delete src build files.
- `run `: Run program with parameters: -i "xxx" -s "xxx" -p "xxx" -t 10 (`i`- device id, `s`- device secret, `p`- product id, `t`- command send interval).
- `run_daemon `: Run program on daemon with parameters: -i "xxx" -s "xxx" -p "xxx" -t 10 -d (`i`- device id, `s`- device secret, `p`- product id, `t`- command send interval, `d`- run with daemon).
- `run_valgrind`: Run program on valgrind with parameters: -i "xxx" -s "xxx" -p "xxx" -t 10 (`i`- device id, `s`- device secret, `p`- product id, `t`- command send interval).

## How to run 

1. Build library and src:
    
    ```
    cd first_daemon_program
    make
    ```
2. Add LD_LIBRARY_PATH:
    Replace {your_path} with path to project (e.g.)`~/projects`:
    
    ```
    LD_LIBRARY_PATH={your_path}/first_daemon_program/lib/tuya-iot-core-sdk/build/lib:$LD_LIBRARY_PATH
    export LD_LIBRARY_PATH
    ```
3. Run program:

    You can run program with default values:

    ```
    make run
    ```
    or use your own values:

    ```
	./src/build/first_daemon_program -i "aaa" -s "bbb" -p "ccc" -t x 
    ```
    (`aaa`- device id, `bbb`- device secret, `ccc`- product id, `x`- (int)command send interval).


## Clouds commands

    To start or stop sending use commands below. Send them to device from tuya IOT platform:

    ### Commands table

    | Command       | Description                   |
    |:--------------|:------------------------------|
    | send_start    | Start sending data to cloud.  |
    | send_stop     | Stop sending data to cloud.   |

## Useful terminal commands

View syslog in termonal;

```
tail -f -n 5 /var/log/syslog | grep "FIRST_DAEMON_PROGRAM"
```