// file become_daemon.c
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include "daemonize.h"

int daemonize()
{
    switch (fork()) // become background process
    {
    case -1:
        return -1;
    case 0:
        break; // child falls through
    default:
        _exit(EXIT_SUCCESS); // parent terminates
    }

    if (setsid() == -1) // become leader of new session
        return -1;

    switch (fork())
    {
    case -1:
        return -1;
    case 0:
        break; // child breaks out of case
    default:
        _exit(EXIT_SUCCESS); // parent process will exit
    }

    return 0;
}