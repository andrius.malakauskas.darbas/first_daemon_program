#include <stdlib.h>
#include <argp.h>
#include "device.h"
#include "daemonize.h"
#include <syslog.h>
#include <unistd.h>
#include <assert.h>
#include <stdio.h>
#include <signal.h>

// need to mention a version string.
const char *argp_program_version = "First daemon program 2.0.0";
const char *LOGNAME = "FIRST_DAEMON_PROGRAM";

// cli argument availble options.
static struct argp_option options[] = {
    {"deviceId", 'i', "", 0, "Device ID"},
    {"deviceSecret", 's', "", 0, "Device secret key"},
    {"productId", 'p', "", 0, "Product ID"},
    {"sendInterval", 't', "", 0, "Data send interval"},
    {"daemonize", 'd', 0, 0, "Run program in daemon."},
    {0}};

// define a function which will parse the args.
static error_t parse_opt(int key, char *arg, struct argp_state *state)
{

    struct Device *arguments = state->input;
    switch (key)
    {

    case 'i':
        arguments->deviceId = arg;
        break;
    case 's':
        arguments->deviceSecret = arg;
        break;
    case 'p':
        arguments->productId = arg;
        break;
    case 't':
        arguments->sendInterval = atoi(arg);
        break;
    case 'd':
        arguments->daemonize = 1;
        break;

    case ARGP_KEY_ARG:

        // Too many arguments.
        if (state->arg_num != 0)
            argp_usage(state);
        break;

    default:
        return ARGP_ERR_UNKNOWN;
    }

    return 0;
}
// initialize the argp struct. Which will be used to parse and use the args.
static struct argp argp = {options, parse_opt};
void sig_handler(int signum)
{
    switch (signum)
    {
    case SIGINT:
        printf("\nCTRL+C pressed.\n");
        tuya_loop_stop();
        break;

    case SIGQUIT:
        printf("\nQuit.\n");
        tuya_loop_stop();
        break;
    case SIGKILL:
        printf("\nKilled.\n");
        tuya_loop_stop();

        break;

    default:
        break;
    }
}

int main(int argc, char *args[])
{
    signal(SIGINT, sig_handler);
    signal(SIGQUIT, sig_handler);
    signal(SIGKILL, sig_handler);
    struct Device arguments;
    int res = 0;
    // set the default values for all of the args.
    arguments.deviceId = "";
    arguments.deviceSecret = "";
    arguments.productId = "";
    arguments.sendInterval = 5;
    arguments.daemonize = 0;

    // parse the cli arguments.
    argp_parse(&argp, argc, args, 0, 0, &arguments);

    // turn this process into a daemon
    if (arguments.daemonize)
    {
        if (daemonize())
        {
            syslog(LOG_USER | LOG_ERR, "error starting");
            closelog();
            return EXIT_FAILURE;
        }
        printf("daemonize\n");
    }
    printf("simple\n");

    openlog(LOGNAME, LOG_PID, LOG_USER);
    syslog(LOG_USER | LOG_INFO, "starting");
    res = device_init(arguments.deviceId, arguments.deviceSecret, arguments.productId, arguments.sendInterval);
    device_deinit();
    return res;
}