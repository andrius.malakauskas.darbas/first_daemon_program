#include <assert.h>
#include <stdio.h>
#include <stdlib.h>
#include <signal.h>
#include <time.h>
#include <sys/time.h>
#include <sys/syslog.h>
#include <string.h>

#include "cJSON.h"
#include "tuya_certificate.h"
// #include "tuya_log.h"
#include "tuya_error_code.h"
#include "system_interface.h"
#include "mqtt_client_interface.h"
#include "tuyalink_core.h"

#define FILE_NAME "data_log.csv"

FILE *file;
tuya_mqtt_context_t client_instance;
struct itimerval timer;
bool is_loop_running = true;
bool is_sending_running = false;
int tuya_loop_stop()
{
    if (!is_loop_running)
    {
        syslog(LOG_USER | LOG_INFO, "Tuya loop already stopped.");
        return -1;
    }
    is_loop_running = false;
    syslog(LOG_USER | LOG_INFO, "Tuya loop stopped.");
    return 0;
}

void timer_callback(int signum)
{
    tuyalink_thing_property_report_with_ack(&client_instance, NULL, "{\"send_data\":{\"value\":\"test\",\"time\":1631708204231}}");
}

int timer_start()
{
    int ret;
    if (is_sending_running)
    {
        syslog(LOG_USER | LOG_INFO, "DEVICE Sending is already started.");
        return -1;
    }
    ret = setitimer(ITIMER_REAL, &timer, NULL);
    if (ret == -1)
    {
        syslog(LOG_USER | LOG_INFO, "DEVICE Failed to start timer");
    }
    syslog(LOG_USER | LOG_INFO, "DEVICE Sending started");
    is_sending_running = true;
    return ret;
}
int timer_stop()
{
    int ret;
    if (!is_sending_running)
    {
        syslog(LOG_USER | LOG_INFO, "DEVICE Sending is already stopped.");
        return -1;
    }
    ret = setitimer(ITIMER_REAL, 0, NULL);
    if (ret == -1)
    {
        syslog(LOG_USER | LOG_INFO, "DEVICE Failed to stop timer");
    }
    syslog(LOG_USER | LOG_INFO, "DEVICE Sending stopped");
    is_sending_running = false;

    return ret;
}

void on_connected(tuya_mqtt_context_t *context, void *user_data)
{
    syslog(LOG_USER | LOG_INFO, "MQTT Connected to TUYA");
    tuyalink_subdevice_bind(context, "[{\"productId\":\"bkgdyaysaiyyqeia\",\"nodeId\":\"123455\",\"clientId\":\"123455asdf\"}]");
    tuyalink_subdevice_topo_get(context);

    timer_start();
}

void on_messages(tuya_mqtt_context_t *context, void *user_data, const tuyalink_message_t *msg)
{
    switch (msg->type)
    {
    // Message get case
    case THING_TYPE_PROPERTY_SET_RSP:
        syslog(LOG_USER | LOG_INFO, "Got message: %s", msg->data_string);
        time_t tm;
        time(&tm);
        file = fopen(FILE_NAME, "a");
        fprintf(file, "%.19s||%s\n", ctime(&tm), msg->data_string);
        fclose(file);
        if (strstr(msg->data_string, "send_stop") != NULL)
            timer_stop();
        if (strstr(msg->data_string, "send_start") != NULL)
            timer_start();
        break;
    case THING_TYPE_PROPERTY_REPORT_RSP:
        syslog(LOG_USER | LOG_INFO, "Data send successfully.");
    default:
        break;
    }
    printf("\r\n");
}

int device_deinit()
{
    tuya_mqtt_disconnect(&client_instance);
    tuya_mqtt_deinit(&client_instance);
}

int device_init(char *deviceId, char *deviceSecret, char *productId, int send_interval)
{
    int ret;

    // Init timer
    timer.it_value.tv_sec = send_interval;
    timer.it_value.tv_usec = 0;
    timer.it_interval.tv_sec = send_interval;
    timer.it_interval.tv_usec = 0;
    signal(SIGALRM, timer_callback);

    tuya_mqtt_init(&client_instance, &(const tuya_mqtt_config_t){
                                         .host = "m1.tuyacn.com",
                                         .port = 8883,
                                         .cacert = tuya_cacert_pem,
                                         .cacert_len = sizeof(tuya_cacert_pem),
                                         .device_id = deviceId,
                                         .device_secret = deviceSecret,
                                         .keepalive = 100,
                                         .timeout_ms = 2000,
                                         .on_connected = on_connected,
                                         .on_messages = on_messages});

    if (tuya_mqtt_connect(&client_instance) != OPRT_OK)
    {
        syslog(LOG_USER | LOG_INFO, "MQTT Connection failed.");
        return -1;
    }

    while (is_loop_running)
    {
        /* Loop to receive packets, and handles client keepalive */
        int x = tuya_mqtt_loop(&client_instance);
    }

    return ret;
}
