#ifndef __DEVICE_H__
#define __DEVICE_H__

struct Device
{
    char *deviceId;
    char *deviceSecret;
    char *productId;
    int sendInterval;
    int daemonize;
};

int device_init(char *deviceId, char *deviceSecret, char *productId, int send_interval);
int device_deinit();

int tuya_loop_stop();
#endif // __DEVICE_H__
